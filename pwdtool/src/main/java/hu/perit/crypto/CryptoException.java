
/*
 * Copyright header
 * The ultimate spring based webservice template project.
 * Author Peter Nagy <nagy.peter.home@gmail.com>
 */

package hu.perit.crypto;

public class CryptoException extends Exception
{
    public CryptoException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
